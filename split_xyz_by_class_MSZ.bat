@echo off
setlocal enabledelayedexpansion
for %%f in ("*.xyz") do (

	REM process file line by line (ignore header):
	for /f "delims=" %%A in (%%f) do (
		for /f "tokens=4 delims=	" %%B in ("%%A") do (
		if not exist "%%~nf_%%B.xyz" echo "%%~nf_%%B.xyz"
		>>"%%~nf_%%B.xyz" echo %%A
		)
	)
)